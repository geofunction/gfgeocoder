package it.geofunction.gfgeocoder.utilities;

/**
 * Created by fabior on 10/07/16.
 */
public class Common {

    public static String TXT_EXT = "txt";

    public static String CSV_EXT = "csv";

    public static String OUT_EXT = "out";

    public static String BAK_EXT = "bak";

    public static int SUCCESS_EXIT_COD = 0;

    public static int ARGS_EXIT_COD = 1;

    public static int NOT_VALID_FILENAME_EXIT_COD = 2;

    public static int FILE_NOT_EXISTS_EXIT_COD = 3;

    public static int INPUT_FILE_NOT_SUPPORTED_EXIT_COD = 4;

    //public static int ERROR_LIMIT = 40;
}