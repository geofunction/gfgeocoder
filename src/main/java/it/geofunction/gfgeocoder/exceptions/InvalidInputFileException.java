/*
 *  InvalidHtmlException.java
 */

package it.geofunction.gfgeocoder.exceptions;

/**
 *
 * @author Fabio Rinnone
 */
public class InvalidInputFileException extends Exception {

	private int line;

	public InvalidInputFileException(int line) {
		this.line = line;
	}

	@Override
	public String getMessage() {
		return "Invalid input file.\nParsing error at line: "+line+"\n";
	}
}
