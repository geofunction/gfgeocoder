package it.geofunction.gfgeocoder.exceptions;

/**
 * Created by fabior on 03/03/17.
 */
public class ArgumentsException extends Exception {

    @Override
    public String getMessage() {
        String message = "Usage: java -jar gfgeocoder.jar file.txt";

        return message;
    }
}
