package it.geofunction.gfgeocoder.writer;

import java.io.File;

/**
 * Created by fabior on 24/03/16.
 */
public class OutWriter implements Writer {

    private static final String EXT = "out";

    private String inputFileName;
    private String outputFileName;

    public OutWriter(String inputFileName) {
        this.inputFileName = inputFileName;
        outputFileName = inputFileName.replace("txt", "out");
    }

    public void write() {
        File fileOutput = new File(outputFileName);
    }
}
