package it.geofunction.gfgeocoder.writer;

/**
 * Created by fabior on 24/03/16.
 */
public interface Writer {

    public void write();
}
