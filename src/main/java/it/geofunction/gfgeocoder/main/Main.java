/*
 * Copyright (c) 2013-2017 Geofunction Srls
 *
 * Eng. Gianfranco Di Pietro
 * Geofunction Srls
 * dipietro@geofunction.it
 * http://www.geofunction.it
 *
 * Dr. Fabio Rinnone
 * Geofunction Srls
 * fabiorinnone@geofunction.it
 * http://www.fabiorinnone.eu
 *
 */

package it.geofunction.gfgeocoder.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import it.geofunction.gfgeocoder.exceptions.ArgumentsException;
import it.geofunction.gfgeocoder.exceptions.InvalidInputFileException;
import it.geofunction.gfgeocoder.geocoder.*;
import it.geofunction.gfgeocoder.utilities.Common;
import it.geofunction.gfgeocoder.utilities.Utilities;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private static Logger LOGGER = LoggerFactory.getLogger("Main");

	private static int GOOGLE_SUCCESS;
	private static int GOOGLE_FAILED;
	//private static int ORS_SUCCESS;
	//private static int ORS_FAILED;
	private static int MAPQUEST_SUCCESS;
	private static int MAPQUEST_FAILED;
	private static int NOMINATIM_SUCCESS;
	private static int NOMINATIM_FAILED;
	private static int BING_SUCCESS;
	private static int BING_FAILED;

	private static GoogleGeocoder googleGeocoder;
	//private static ORSGeocoder orsGeocoder;
	private static MapQuestGeocoder mapQuestGeocoder;
	private static NominatimGeocoder nominatimGeocoder;
	private static BingGeocoder bingGeocoder;

	public static void main(String[] args) {

		ManageArgs manageArgs = null;

		try {
			manageArgs = new ManageArgs(args);
		} catch (ArgumentsException e) {
			System.err.println(e.getMessage());
			System.exit(Common.ARGS_EXIT_COD);
		}

		String fileInputName = "";
		if (manageArgs.getFileInputName() != null)
			fileInputName = manageArgs.getFileInputName();

		if (fileInputName.equals("")) {
			LOGGER.error("Filename not valid");
			System.exit(Common.NOT_VALID_FILENAME_EXIT_COD);
		}

		File fileInput = new File(fileInputName);
		if (!fileInput.exists()) {
			LOGGER.error("File " + fileInputName + " not exists");
			System.exit(Common.FILE_NOT_EXISTS_EXIT_COD);
		}

		LOGGER.info("Welcome to GFgeocoder. Copyright(c) 2013-2017 Geofunction Srls");
		LOGGER.info("Sources codes are available here: https://bitbucket.org/geofunction/gfgeocoder");
		LOGGER.info("See README.md and LICENSE files for information about usage of software");

		LOGGER.info("Geocoding " + fileInputName);

		String fileOutputName = null;

		if (fileInputName.endsWith(Common.TXT_EXT))
			fileOutputName = fileInputName.replace(Common.TXT_EXT, Common.OUT_EXT);
		else if (fileInputName.endsWith(Common.CSV_EXT))
			fileOutputName = fileInputName.replace(Common.CSV_EXT, Common.OUT_EXT);
		else {
			LOGGER.error("Input file not supported");
			System.exit(Common.INPUT_FILE_NOT_SUPPORTED_EXIT_COD);
		}

		File fileOutput = new File(fileOutputName);

		int addressesGeocoded = 0;
		try {
			addressesGeocoded = Utilities.getAddressesGeocodedFromOutputFile(fileOutput);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileBackupName = fileOutputName.concat("." + Common.BAK_EXT);
		if (fileOutput.exists()) {
			File fileBackup = new File(fileBackupName);
			try {
				FileUtils.copyFile(fileOutput, fileBackup);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		googleGeocoder = GoogleGeocoder.getInstance();
		//orsGeocoder = ORSGeocoder.getInstance();
		mapQuestGeocoder = MapQuestGeocoder.getInstance();
		nominatimGeocoder = NominatimGeocoder.getInstance();
		bingGeocoder = BingGeocoder.getInstance();

		GOOGLE_SUCCESS = GOOGLE_FAILED =  0;
		//ORS_SUCCESS = ORS_FAILED = 0;
		MAPQUEST_SUCCESS = MAPQUEST_FAILED = 0;
		NOMINATIM_SUCCESS = NOMINATIM_FAILED = 0;
		BING_SUCCESS = BING_FAILED = 0;

		try {
			geocode(fileInput, fileOutput, addressesGeocoded);
		} catch (InvalidInputFileException e) {
			System.out.println(e.getMessage());
		}

		System.exit(Common.SUCCESS_EXIT_COD);
	}

	private static void geocode(File fileInput, File fileOutput, int addressesGeocoded) throws InvalidInputFileException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileInput));
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput, true));
			int lineNumber = addressesGeocoded;
			String line;

			int numColumns = -1;

			int countLines = 0;
			while (countLines <= addressesGeocoded) {
				reader.readLine();
				countLines++;
			}

			while ((line = reader.readLine()) != null) {
				writer = new BufferedWriter(new FileWriter(fileOutput, true));
				lineNumber++;

				if (lineNumber == 1) {
					String[] lineSplitted = line.split("#");
					String firstLine = "City#Address#GoogleLat#GoogleLon#ORSLat#ORSLon#" +
							"MapQuestLat#MapQuestLon#NominatimLat#NominatimLon#BingLat#BingLon\n";

					if (lineSplitted.length == 3)
						firstLine = "ID#" + firstLine;
					else if (lineSplitted.length != 2 && lineSplitted.length != 3)
						throw new InvalidInputFileException(1);

					//numColumns = lineSplitted.length;

					writer.write(firstLine);
					writer.close();
				}

				try { //issue #19
					if (lineNumber > 1) {
						String[] lineSplitted = line.split("#");

						numColumns = lineSplitted.length;

						if (lineSplitted.length != numColumns)
							throw new InvalidInputFileException(lineNumber);

						String id = "";
						String indirizzo = "";
						String comune = "";

						if (numColumns == 2) {
							indirizzo = lineSplitted[0];
							comune = lineSplitted[1];
						} else {
							id = lineSplitted[0];
							indirizzo = lineSplitted[1];
							comune = lineSplitted[2];
						}

						MyCoord googleCoord = new MyCoord(Double.NaN, Double.NaN);
						MyCoord orsCoord = new MyCoord(Double.NaN, Double.NaN);
						MyCoord mapQuestCoord = new MyCoord(Double.NaN, Double.NaN);
						MyCoord nominatimCoord = new MyCoord(Double.NaN, Double.NaN);
						MyCoord bingCoord = new MyCoord(Double.NaN, Double.NaN);

						try {
							googleCoord = googleGeocoder.geocode(comune, indirizzo);
							//orsCoord = orsGeocoder.geocode(comune, indirizzo);
							mapQuestCoord = mapQuestGeocoder.geocode(comune, indirizzo);
							nominatimCoord = nominatimGeocoder.geocode(comune, indirizzo);
							bingCoord = bingGeocoder.geocode(comune, indirizzo);
						} catch (Exception e) {
							LOGGER.error("Geocoder limit probably exceeded");
							e.printStackTrace();
							Utilities.sleepGeocoder(8);
						}

						try {
							TimeUnit.SECONDS.sleep(2);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						if (googleCoord.isValid())
							GOOGLE_SUCCESS++;
						else
							GOOGLE_FAILED++;

						/*if (orsCoord.isValid())
							ORS_SUCCESS++;
						else
							ORS_FAILED++;*/

						if (mapQuestCoord.isValid())
							MAPQUEST_SUCCESS++;
						else
							MAPQUEST_FAILED++;

						if (nominatimCoord.isValid())
							NOMINATIM_SUCCESS++;
						else
							NOMINATIM_FAILED++;

						if (bingCoord.isValid())
							BING_SUCCESS++;
						else
							BING_FAILED++;

						String newLine = indirizzo + "#" + comune + "#" +
								googleCoord.getLatitude() + "#" + googleCoord.getLongitude() + "#" +
								//orsCoord.getLatitude() + "#" + orsCoord.getLongitude() + "#" +
								mapQuestCoord.getLatitude() + "#" + mapQuestCoord.getLongitude() + "#" +
								nominatimCoord.getLatitude() + "#" + nominatimCoord.getLongitude() + "#" +
								bingCoord.getLatitude() + "#" + bingCoord.getLongitude() + "\n";

						if (numColumns == 3)
							newLine = id + "#" + newLine;

						LOGGER.info("Add line: " + newLine.replace("\n", ""));
						writer.write(newLine);
						writer.close();
					}
				} catch (InvalidInputFileException e) {
					e.printStackTrace();
				}
			}

			LOGGER.info("Completed!");

			LOGGER.info("Geocoded with Google: " + GOOGLE_SUCCESS);
			LOGGER.info("Failed with Google: " + GOOGLE_FAILED);

			//LOGGER.info("Geocoded with ORS: " + ORS_SUCCESS);
			//LOGGER.info("Failed with ORS: " + ORS_FAILED);

			LOGGER.info("Geocoded with MapQuest: " + MAPQUEST_SUCCESS);
			LOGGER.info("Failed with MapQuest: " + MAPQUEST_FAILED);

			LOGGER.info("Geocoded with Nominatim: " + NOMINATIM_SUCCESS);
			LOGGER.info("Failed with Nominatim: " + NOMINATIM_FAILED);

			LOGGER.info("Geocoded with Bing: " + BING_SUCCESS);
			LOGGER.info("Failed with Bing: " + BING_FAILED);

			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}