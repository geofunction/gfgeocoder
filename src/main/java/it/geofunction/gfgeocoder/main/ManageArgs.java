package it.geofunction.gfgeocoder.main;

import it.geofunction.gfgeocoder.exceptions.ArgumentsException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Created by fabior on 03/03/17.
 */
public class ManageArgs {

    public String fileInputName;

    public ManageArgs(String[] args) throws ArgumentsException {
        fileInputName = null;

        int numArgs = args.length;

        if (numArgs == 0 || numArgs > 1)
            throw new ArgumentsException();

        if (numArgs == 1)
            fileInputName = args[0];
    }

    public String getFileInputName() {
        return fileInputName;
    }
}
