package it.geofunction.gfgeocoder.geocoder;

/**
 * Created by fabior on 21/03/16.
 */
public interface GeocoderInterface {

    public MyCoord geocode(String city, String address) throws Exception;
}
