package it.geofunction.gfgeocoder.geocoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by fabior on 21/09/17.
 */
public class BingGeocoder implements GeocoderInterface {

    private static BingGeocoder instance = null;

    private String scheme;
    private String host;
    private String path;
    private final String key = "Au5yfLshvaRAvTwSAw5OdrYca1X7wcaF3fOPYcdLeyDEDbiXt7cmpV6WnMNM4jrX";

    private BingGeocoder() {
        scheme = "http";
        host = "dev.virtualearth.net";
        path = "/REST/v1/Locations";
    }

    public static synchronized BingGeocoder getInstance() {
        if (instance == null) {
            instance = new BingGeocoder();
        }
        return instance;
    }


    public MyCoord geocode(String city, String address) throws Exception {
        Double lat;
        Double lon;

        String result = getHttpRequest(key, address, city);

        JSONObject jsonObject = new JSONObject(result);
        JSONArray resourceSets = (JSONArray) jsonObject.get("resourceSets");
        JSONObject resourceSets0 = (JSONObject) resourceSets.get(0);
        JSONArray resources = (JSONArray) resourceSets0.get("resources");
        JSONObject resources0 = (JSONObject) resources.get(0);
        JSONObject point = (JSONObject) resources0.get("point");
        JSONArray coordinates = (JSONArray) point.get("coordinates");
        lat = (Double) coordinates.get(0);
        lon = (Double) coordinates.get(1);

        if (lat == null && lon == null)
            return new MyCoord(Double.NaN, Double.NaN);
        else
            return new MyCoord(lat, lon);
    }

    @NotNull
    private String getHttpRequest(String key, String address, String city)
            throws URISyntaxException, IOException {

        String urlStr = scheme +
                "://" + host + path + "?key=" + key + "&locality=" + city + "&addressLine=" + address;
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(),
                url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(uri);
        CloseableHttpResponse response = httpclient.execute(httpget);

        String output = "";
        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    output = IOUtils.toString(instream);
                } finally {
                    instream.close();
                }
            }
        } finally {
            response.close();
        }

        //System.out.println(output);
        return output;
    }
}
