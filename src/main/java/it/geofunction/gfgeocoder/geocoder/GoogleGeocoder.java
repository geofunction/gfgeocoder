package it.geofunction.gfgeocoder.geocoder;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by fabior on 21/03/16.
 */
public class GoogleGeocoder implements GeocoderInterface {

    private static GoogleGeocoder instance = null;

    private Geocoder geocoder;

    private GoogleGeocoder() {
        geocoder = new Geocoder();
    }

    public static synchronized GoogleGeocoder getInstance() {
        if (instance == null) {
            instance = new GoogleGeocoder();
        }
        return instance;
    }

    public MyCoord geocode(String city, String address) throws Exception {
        String request = address +" " + city;

        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder()
                .setAddress(request)
                .setLanguage("it")
                .getGeocoderRequest();
        GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
        GeocoderStatus status = geocoderResponse.getStatus();

        if (status.equals(GeocoderStatus.OK)) {
            List<GeocoderResult> geocoderResults = geocoderResponse.getResults();
            GeocoderResult geocoderResult = geocoderResults.get(0);
            GeocoderGeometry geocoderGeometry = geocoderResult.getGeometry();
            com.google.code.geocoder.model.LatLng location = geocoderGeometry.getLocation();

            BigDecimal latitude = location.getLat();
            BigDecimal longitude = location.getLng();

            Double lat = latitude.doubleValue();
            Double lng = longitude.doubleValue();

            return new MyCoord(lat, lng);
        }
        else if (status.equals(GeocoderStatus.ZERO_RESULTS))
            return new MyCoord(Double.NaN, Double.NaN);
        else throw new Exception();
    }

    public MyCoord geocode(String completeAddress) throws Exception {
        String request = completeAddress;

        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder()
                .setAddress(request)
                .setLanguage("it")
                .getGeocoderRequest();
        GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
        GeocoderStatus status = geocoderResponse.getStatus();
        if (status.equals(GeocoderStatus.OK)) {
            List<GeocoderResult> geocoderResults = geocoderResponse.getResults();
            GeocoderResult geocoderResult = geocoderResults.get(0);
            GeocoderGeometry geocoderGeometry = geocoderResult.getGeometry();
            com.google.code.geocoder.model.LatLng location = geocoderGeometry.getLocation();

            BigDecimal latitude = location.getLat();
            BigDecimal longitude = location.getLng();

            Double lat = latitude.doubleValue();
            Double lng = longitude.doubleValue();

            return new MyCoord(lat, lng);
        }
        else if (status.equals(GeocoderStatus.ZERO_RESULTS))
            return new MyCoord(Double.NaN, Double.NaN);
        else throw new Exception();
    }
}
