package it.geofunction.gfgeocoder.geocoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;

/**
 * Created by fabior on 22/03/16.
 */
public class MapQuestGeocoder implements GeocoderInterface {

    private static MapQuestGeocoder instance = null;

    private String scheme;
    private String host;
    private String path;
    private final String key = "oyqXoynRvLc9OIGxq4XGWAGgAYGmf4sA";

    private MapQuestGeocoder() {
        scheme = "http";
        host = "www.mapquestapi.com";
        path = "/geocoding/v1/address";
    }

    public static synchronized MapQuestGeocoder getInstance() {
        if (instance == null) {
            instance = new MapQuestGeocoder();
        }
        return instance;
    }

    public MyCoord geocode(String city, String address) throws Exception {
        String location = address + " " + city;

        Double lat;
        Double lon;

        String result = getHttpRequest(key, location);

        JSONObject jsonObject = new JSONObject(result);
        JSONArray results = (JSONArray) jsonObject.get("results");
        JSONObject results0 = (JSONObject) results.get(0);
        JSONArray locations = (JSONArray) results0.get("locations");
        JSONObject locations0 = (JSONObject) locations.get(0);
        JSONObject latLng = (JSONObject) locations0.get("latLng");
        lat = latLng.getDouble("lat");
        lon = latLng.getDouble("lng");

        if (lat == null && lon == null)
            return new MyCoord(Double.NaN, Double.NaN);
        else
            return new MyCoord(lat, lon);
    }

    @NotNull
    private String getHttpRequest(String key, String location)
            throws URISyntaxException, IOException {

        String urlStr = scheme +
                "://" + host + path + "?key=" + key + "&location=" + location;
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(),
                url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(uri);
        CloseableHttpResponse response = httpclient.execute(httpget);

        String output = "";
        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    output = IOUtils.toString(instream);
                } finally {
                    instream.close();
                }
            }
        } finally {
            response.close();
        }

        //System.out.println(output);
        return output;
    }
}
