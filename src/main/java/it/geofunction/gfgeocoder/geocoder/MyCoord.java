package it.geofunction.gfgeocoder.geocoder;

/**
 * Created by fabior on 21/03/16.
 */
public class MyCoord {

    private Double latitude;
    private Double longitude;

    public MyCoord(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public boolean isValid() {
        if (Double.isNaN(latitude) || Double.isNaN(longitude))
            return false;
        //if (latitude == null || longitude == null)
            //return false;
        else
            return true;
    }

    @Override
    public String toString() {
        return "(" + latitude + ";" + longitude + ")";
    }
}
