package it.geofunction.gfgeocoder.geocoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;

/**
 * Created by fabior on 21/03/16.
 */
public class ORSGeocoder implements GeocoderInterface {

    private static final String API_KEY = "ee0b8233adff52ce9fd6afc2a2859a28";

    private static ORSGeocoder instance = null;

    private String scheme;
    private String host;
    private String path;

    private ORSGeocoder() {
        scheme = "http";
        host = "openls.geog.uni-heidelberg.de";
        path = "/geocode";
    }

    public static synchronized ORSGeocoder getInstance() {
        if (instance == null) {
            instance = new ORSGeocoder();
        }
        return instance;
    }

    public MyCoord geocode(String city, String address) throws Exception {
        String freeFormAdress = address +  ", " + city;
        int maxResponse = 1;

        Double lat = null;
        Double lon = null;

        String result = getHttpRequest(freeFormAdress, maxResponse);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        InputStream is = new ByteArrayInputStream(result.getBytes());
        Document doc = dBuilder.parse(is);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("gml:pos");

        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            String content = nNode.getTextContent();
            String[] splitContent = content.split(" ");

            lat = Double.parseDouble(splitContent[1]);
            lon = Double.parseDouble(splitContent[0]);
        }

        if (lat == null && lon == null)
            return new MyCoord(Double.NaN, Double.NaN);
        else
            return new MyCoord(lat, lon);
    }

    @NotNull
    private String getHttpRequest(String freeFormAdress, int maxResponse)
            throws URISyntaxException, IOException {
        String urlStr = scheme +
                "://" + host + path + "?FreeFormAddress=" + freeFormAdress + "&MaxResponse=" + maxResponse +
                "&api_key=" + API_KEY;
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(),
                url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(uri);
        CloseableHttpResponse response = httpclient.execute(httpget);

        String output = "";
        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    output = IOUtils.toString(instream);
                } finally {
                    instream.close();
                }
            }
        } finally {
            response.close();
        }

        return output;
    }
}
