package it.geofunction.gfgeocoder.geocoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by fabior on 05/03/17.
 */
public class NominatimGeocoder implements GeocoderInterface {

    private static NominatimGeocoder instance = null;

    private String scheme;
    private String host;
    private String path;

    private NominatimGeocoder() {
        scheme = "http";
        host = "nominatim.openstreetmap.org";
        path = "/search";
    }

    public static synchronized NominatimGeocoder getInstance() {
        if (instance == null) {
            instance = new NominatimGeocoder();
        }
        return instance;
    }

    @Override
    public MyCoord geocode(String city, String address) throws Exception {
        String location = address + " " + city;

        Double lat = null;
        Double lon = null;

        String result = getHttpRequest(location);

        JSONArray jsonArray = new JSONArray(result);

        try {
            JSONObject results0 = (JSONObject) jsonArray.get(0);
            lat = results0.getDouble("lat");
            lon = results0.getDouble("lon");
        } catch (JSONException e) {
            // no op
        }
        
        if (lat == null && lon == null)
            return new MyCoord(Double.NaN, Double.NaN);
        else
            return new MyCoord(lat, lon);
    }

    private String getHttpRequest(String location)
            throws URISyntaxException, IOException {

        String urlStr = scheme +
                "://" + host + path + "/" + location + "?format=json&addressdetails=1&limit=1&polygon_svg=1";
        URL url = new URL(urlStr);
        URI uri = new URI(url.getProtocol(),
                url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(uri);
        CloseableHttpResponse response = httpclient.execute(httpget);

        String output = "";
        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    output = IOUtils.toString(instream);
                } finally {
                    instream.close();
                }
            }
        } finally {
            response.close();
        }

        return output;
    }
}
