# README #

GFgeocoder is a tool written in Java language that allow to make parallel requests to some Online Geocoding Services, passing in input the files containing strings of addresses collected. Obtained results are stored in tabular mode, in CSV format. 

GFgeocoder is a multiplatform tool executable from command line and can be run as a background daemon. GFgeocoder allows to load input CSV file that contains two columns: first column, named address, must contains addresses and house numbers, and second column, named city, must contains city name. 

Output CSV file contains more than two columns: the columns named address and city are identical to input file columns; the other columns contain respectively pairs of coordinates (latitude and longitude) obtained as results from Online Geocoders Services.

GFgeocoder use following Online Geocoding Services:

* Google: https://developers.google.com/maps/documentation/geocoding/intro
* MapQuest: https://developer.mapquest.com/documentation/geocoding-api/
* Nominatim: http://wiki.openstreetmap.org/wiki/Nominatim

### Usage ###

Download jar file and execute:

java -jar gfgeocoder-VERSION.jar file.csv

where file.csv contains addresses and city names formatted as describe above.

Character Set: UTF-8
filed delimiter: hash sign ```#```

Sample file.csv
```
id#name#adress#city
01#Geofunction#Via L. Sturzo 71#Niscemi
```

### Term of use ###

This software is released under Apache Licence Version 2.0: see LICENSE for further details.

This software uses several external APIs. See following links for further details about their terms of use:

* Google Maps APIs: https://developers.google.com/maps/terms
* MapQuest: https://hello.mapquest.com/terms-of-use
* OpenRouteService: https://operations.osmfoundation.org/policies/nominatim/

Note: OpenRouteService geocoder is not longer supported.